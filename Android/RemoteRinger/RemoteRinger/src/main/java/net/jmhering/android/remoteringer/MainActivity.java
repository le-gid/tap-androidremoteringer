package net.jmhering.android.remoteringer;

import net.jmhering.android.remoteringer.util.SystemUiHider;
import net.jmhering.android.remoteringer.util.TheatreMagic;
import net.jmhering.android.remoteringer.util.TheatreServer;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Vibrator;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.net.wifi.*;
import android.media.*;
import com.illposed.osc.*;

import java.net.Socket;
import java.net.SocketException;
import java.util.Formatter;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 *
 * @see SystemUiHider
 */
public class MainActivity extends Activity {
    /**
     * Whether or not the system UI should be auto-hidden after
     * {@link #AUTO_HIDE_DELAY_MILLIS} milliseconds.
     */
    private static final boolean AUTO_HIDE = true;

    /**
     * If {@link #AUTO_HIDE} is set, the number of milliseconds to wait after
     * user interaction before hiding the system UI.
     */
    private static final int AUTO_HIDE_DELAY_MILLIS = 3000;

    /**
     * If set, will toggle the system UI visibility upon interaction. Otherwise,
     * will show the system UI visibility upon interaction.
     */
    private static final boolean TOGGLE_ON_CLICK = true;

    /**
     * The flags to pass to {@link SystemUiHider#getInstance}.
     */
    private static final int HIDER_FLAGS = SystemUiHider.FLAG_HIDE_NAVIGATION;

    /**
     * Used for OSC. Instance of TheatreMagic.
     */
    private TheatreMagic tm;

    /**
     * EditText that a log is written to.
     */
    private EditText logText;

    /**
     * OSCPortIn used to listen for OSC commands.
     */
    private OSCPortIn receiver;

    /**
     * The instance of the {@link SystemUiHider} for this activity.
     */
    private SystemUiHider mSystemUiHider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        View contentView = findViewById(R.id.frameLayout);

        // Set up an instance of SystemUiHider to control the system UI for
        // this activity.
        mSystemUiHider = SystemUiHider.getInstance(this, contentView, HIDER_FLAGS);
        mSystemUiHider.setup();
        mSystemUiHider
                .setOnVisibilityChangeListener(new SystemUiHider.OnVisibilityChangeListener() {
                    // Cached values.
                    int mControlsHeight;
                    int mShortAnimTime;

                    @Override
                    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
                    public void onVisibilityChange(boolean visible) {
                        if (visible && AUTO_HIDE) {
                            // Schedule a hide().
                            delayedHide(AUTO_HIDE_DELAY_MILLIS);
                        }
                    }
                });

        // Set up the user interaction to manually show or hide the system UI.
        contentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TOGGLE_ON_CLICK) {
                    mSystemUiHider.toggle();
                } else {
                    mSystemUiHider.show();
                }
            }
        });

        // Assign logText to an instantiated object
        logText = (EditText) findViewById(R.id.logText);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        /**
         * Trigger the initial hide() shortly after the activity has been created, to briefly hint
         * to the user that UI controls.
         */

        delayedHide(100);

        // For debugging purpose.
        logText.append("App started. Please choose server mode.\n");
        Log.e("SERVICE", "App started.");

        // The user needs to know the IP address of the device.
        WifiManager wifiMgr = (WifiManager) getSystemService(WIFI_SERVICE);
        WifiInfo wifiInfo = wifiMgr.getConnectionInfo();
        int ip = wifiInfo.getIpAddress();

        // Format the IP address and print it to the user.
        String ipAddress = android.text.format.Formatter.formatIpAddress(ip);
        logText.append("IP address: " + ipAddress + "\n");
    }


    /**
     * Touch listener to use for in-layout UI controls to delay hiding the
     * system UI. This is to prevent the jarring behavior of controls going away
     * while interacting with activity UI.
     */
    View.OnTouchListener mDelayHideTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (AUTO_HIDE) {
                delayedHide(AUTO_HIDE_DELAY_MILLIS);
            }
            return false;
        }
    };


    /**
     * Can be used to stop the OSC server.
     * @param view The view calling the function. Can be used for Buttons.
     */
    public void stopOSCServer(View view) {
        if (receiver != null && receiver.isListening()) {
            receiver.stopListening();
            logText.append("Stopped OSC server\n");
        }
        else {
            logText.append("OSC not running!\n");
        }
    }

    /**
     * Start the OSC server and wait for incoming commands.
     * @param view The view calling the function. Can be used for Buttons.
     */
    public void startOSCServer(View view) {
        // Debug information.
        logText.append("Starting OSC server (5400; UDP)...");

        // We only need one instance of TheatreMagic.
        if (tm == null) {
            tm = new TheatreMagic((Vibrator) getSystemService(Context.VIBRATOR_SERVICE), (WifiManager) getSystemService(WIFI_SERVICE), this, (AudioManager) getSystemService(AUDIO_SERVICE));
        }

        // Start the actual OSC server.
        try {
            receiver = new OSCPortIn(5400);

            /**
             * Define OSCListeners that are called when a command is recognized.
             */


            /**
             * Starts the playback.
             */
            OSCListener listener_play = new OSCListener() {
                public void acceptMessage(java.util.Date time, OSCMessage message) {
                    Log.e("OSC", "PLAY");
                    tm.doMagic();
                }
            };

            /**
             * Stops the playback.
             */
            OSCListener listener_stop = new OSCListener() {
                public void acceptMessage(java.util.Date time, OSCMessage message) {
                    Log.e("OSC", "STOP");
                    tm.stopAll();
                }
            };

            /**
             * Stops the OSC server.
             */
            OSCListener listener_exit = new OSCListener() {
                public void acceptMessage(java.util.Date time, OSCMessage message) {
                    Log.e("OSC", "EXIT");
                    receiver.stopListening();
                }
            };

            /**
             * Gets a heartbeat to keep the connection alive. Actually does nothing. Necessary
             * to keep compatibility to QLab (disconnects after 31s of no-message).
             */
            OSCListener listener_heartbeat = new OSCListener() {
                public void acceptMessage(java.util.Date time, OSCMessage message) {
                    long currentTime = System.nanoTime();
                    Log.e("OSC", "GOT HEARTBEAT " + currentTime);
                }
            };


            /**
             * Map listeners to commands.
             */
            receiver.addListener("/play", listener_play);
            receiver.addListener("/stop", listener_stop);
            receiver.addListener("/hb", listener_heartbeat);
            receiver.addListener("/exit", listener_exit);

            /**
             * Start the server.
             */
            receiver.startListening();

            /**
             * Everything is ok.
             */
            if (receiver.isListening()) {
                logText.append(" done.\n");
            }
        }

        /**
         * Maybe the port already is in use or network is unavailable?
         */
        catch (SocketException e) {
            logText.append("\n ERROR: " + e.getMessage() + "\n");
            e.printStackTrace();
        }
    }

    /**
     * Start the TCP socket server.
     * @param view The view calling the function. Can be used for Buttons.
     */
    public void startTCPServer(View view) {
        // For debugging.
        logText.append("Starting TCP server... ");

        // Start as a service.
        Intent i = new Intent(this, TheatreServer.class);
        this.startService(i);

        // For debugging again.
        logText.append("done\n");
    }


    Handler mHideHandler = new Handler();
    Runnable mHideRunnable = new Runnable() {
        @Override
        public void run() {
            mSystemUiHider.hide();
        }
    };

    /**
     * Schedules a call to hide() in [delay] milliseconds, canceling any
     * previously scheduled calls.
     */
    private void delayedHide(int delayMillis) {
        mHideHandler.removeCallbacks(mHideRunnable);
        mHideHandler.postDelayed(mHideRunnable, delayMillis);
    }

}
