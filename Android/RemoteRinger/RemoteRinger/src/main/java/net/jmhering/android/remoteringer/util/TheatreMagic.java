package net.jmhering.android.remoteringer.util;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Looper;
import android.os.Vibrator;
import android.util.Log;

import net.jmhering.android.remoteringer.R;

import java.io.IOException;
import java.io.Serializable;

/**
 * Created by mirko on 27.03.14.
 */
public class TheatreMagic {

    private static String RINGTONE_FILENAME = "";

    /**
     * Objects that can only be instanciated in MainActivity.jar
     */
    private WifiManager wifiManager;
    private Vibrator vibrator;
    private Context context;
    private AudioManager audioManager;
    private AsyncTask vibrations;
    private MediaPlayer mediaPlayer;

    private boolean ringtoneLoaded = false;
    private int ringtoneID;

    /**
     * Get all the instances of objects that can only be created in MainActivity.
     * @param v
     * @param w
     */
    public TheatreMagic(Vibrator v, WifiManager w, Context c, AudioManager a) {
        this.wifiManager = w;
        this.vibrator = v;
        this.context = c;
        this.mediaPlayer = MediaPlayer.create(context, R.raw.sexy);
        this.audioManager = a;
    }

    /**
     * used to start everythin at once.
     */
    public void doMagic() {
        playRingtone();
        //vibrations = new Vibrations().execute("bli bla blubb");
    }

    /**
     * counterpart to doMagic(): Stops everything at once.
     */
    public void stopAll() {
        stopRingtone();
        if (vibrations != null) {
            vibrations.cancel(true);
        }
    }

    /**
     * Makes the phone vibrate.
     */
    protected void vibrate() {
        Log.e("MAGIC", "Starting vibration");
        try {
            int i = 30;
            for (int n = 0; n < i; n++) {
                this.vibrator.vibrate(500);
                Thread.sleep(750);
            }
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Stops the ringtone and resets the mediaPlayer so that it can be started again right away.
     */
    protected void stopRingtone() {
        try {
            mediaPlayer.stop();
            mediaPlayer.prepare();
            mediaPlayer.seekTo(0);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Prepares mediaPlayer so that it is always in a ready state.
     */
    public void stayReady() {
        if (!mediaPlayer.isPlaying()) {
            try {
                mediaPlayer.prepare();
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Starts the ringtone playing and sets the volume.
     */
    protected void playRingtone() {
        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC,audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC), 0);
        mediaPlayer.setVolume(1, 1);
        mediaPlayer.setLooping(true);
        mediaPlayer.start();
    }

    /**
     * Get the IP address of the wifi adapter.
     * Ip address = 0.0.0.0 if not connected!
     * @return
     */
    public String getIpAddr() {
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        int ip = wifiInfo.getIpAddress();

        String ipString = String.format(
                "%d.%d.%d.%d",
                (ip & 0xff),
                (ip >> 8 & 0xff),
                (ip >> 16 & 0xff),
                (ip >> 24 & 0xff));

        return ipString;
    }

    /**
     * AsyncTask used so that the vibrations can happen at the same time as music/ringtone plays.
     */
    class Vibrations extends AsyncTask<String, Void, Void> {
        @Override
        protected Void doInBackground(String... voids) {
            vibrate();
            return null;
        }

        protected void onCancelled() {
            vibrator.cancel();
        }
    }

}
