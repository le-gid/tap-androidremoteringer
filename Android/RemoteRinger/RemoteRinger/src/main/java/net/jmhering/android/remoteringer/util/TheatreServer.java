package net.jmhering.android.remoteringer.util;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Looper;
import android.os.Vibrator;
import android.util.Log;
import android.widget.EditText;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;

/**
 * Created by mirko on 27.03.14.
 */
public class TheatreServer extends Service {

    private TheatreMagic tm;
    private ServerSocket serverSocket;

    Thread serverThread = null;
    public static final int SERVERPORT = 6000;


    /**
     * Makes the service stay sticky. This means that it will be restarted if it stops. May be
     * run several times during service creation. See Android documentation.
     * @param i Intent.
     * @param flags int.
     * @param startId int.
     * @return Service.START_STICKY.
     */
    public int onStartCommand(Intent i, int flags, int startId) {
        Log.e("SERVICE", "STICKY");
        return Service.START_STICKY;
    }

    /**
     * Ran once when the service is started. Instantiates TheatreMagic and starts a ServerThread to
     * wait for incoming connections.
     */
    public void onCreate() {
        Log.e("SERVICE", "onCreate");
        tm = new TheatreMagic((Vibrator) getSystemService(Context.VIBRATOR_SERVICE), (WifiManager) getSystemService(WIFI_SERVICE), this, (AudioManager) getSystemService(AUDIO_SERVICE));
        this.serverThread = new Thread(new ServerThread());
        this.serverThread.start();
    }

    /**
     * Ran when the service is being stopped. Sends interrupt signal to ServerThread.
     */
    public void onDestroy() {
        Log.e("SERVICE", "Service destroyed!");
        this.serverThread.interrupt();
    }


    /**
     * Not used
     * @param intent Intent.
     * @return null.
     */
    public IBinder onBind(Intent intent) {
        //TODO for communication return IBinder implementation
        return null;
    }

    /**
     *
    public void stop() {
        try {
            serverSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    */

    /**
     * Handles the server socket and handles new incoming connections.
     */
    class ServerThread implements Runnable {

        /**
         * Performs the task of waiting for a client to connect. As soon as a client connects, it
         * will be assigned a client-socket, a CommunicationThread and a KeepAliveThread.
         */
        public void run() {
            Socket socket = null;
            try {
                /**
                 * Starts new server socket.
                 */
                serverSocket = new ServerSocket(SERVERPORT);
                Log.e("NETWORKING", "Server running!");
            } catch (IOException e) {
                /**
                 * Might occur when server port already in use.
                 */
                e.printStackTrace();
            }
            while (!Thread.currentThread().isInterrupted()) {

                try {
                    /**
                     * Accept the incoming connection and get the client socket.
                     */
                    socket = serverSocket.accept();
                    Log.i("NETWORKING", "Someone connected from " + socket.getInetAddress());

                    /**
                     * Start the threads to run CommunicationThread and KeepAliveThread
                     * simultaneously.
                     */

                    CommunicationThread commThread = new CommunicationThread(socket);
                    KeepAliveThread kaThread = new KeepAliveThread(socket);

                    new Thread(commThread).start();
                    new Thread(kaThread).start();

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Handles the connection between client and server. Reads input to fetch commands passed to
     * the server and submits responses.
     */
    class CommunicationThread implements Runnable {

        private Socket clientSocket;

        private BufferedReader input;
        private PrintWriter output;

        /**
         * Constructor.
         * @param clientSocket The client socket to talk to.
         */
        public CommunicationThread(Socket clientSocket) {
            this.clientSocket = clientSocket;
            /**
             * Establish input and output streams on the client socket.
             */
            try {
                this.input = new BufferedReader(new InputStreamReader(this.clientSocket.getInputStream()));
                this.output = new PrintWriter(this.clientSocket.getOutputStream(), true);
                printOut("Con OK");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        /**
         * Performs the task of readling input passed by client and starts functions according to
         * the command entered.
         */
        public void run() {
            Looper.prepare();
            while (!Thread.currentThread().isInterrupted()) {
                /**
                 * Read lines from BufferedReader input.
                 * Will be executed everytime a \n is submitted.
                 */
                try {
                    /**
                     * I think this might be unnecessary as NullPointerExceptions are handled via
                     * try-catch. But: one never knows ;)
                     */
                    if (input == null) {
                        Thread.currentThread().interrupt();
                        Looper.myLooper().quit();
                        break;
                    }
                    else {
                        /**
                         * Read line from input.
                         */
                        String read = input.readLine().trim().toLowerCase();

                        /**
                         * Maybe the client wants to close the connection properly.
                         */
                        if (read.equals("exit")) {
                            printOut("exit OK");
                            Thread.currentThread().interrupt();
                            clientSocket.close();
                        }

                        /**
                         * Maybe the client wants the android system to make some music?
                         */
                        else if (read.equals("play")) {
                            printOut("play OK");
                            tm.doMagic();
                        }

                        /**
                         * Maybe it was enough music and the system shall shut up..
                         */
                        else if (read.equals("stop")) {
                            printOut("stop OK");
                            tm.stopAll();
                        }

                        /**
                         * Whatever.
                         */
                        else {
                            printOut("unknown ER");
                        }
                    }
                } catch (IOException e) {
                    Log.e("NETWORKING", "LOST CONNECTION!", e);

                } catch (NullPointerException e) {
                    /**
                     * This mainly occurs when the client window is being closed without properly
                     * shutting down the tcp-connection.
                     */
                    Log.e("NETWORKING", "LOST CONNECTION NULLPOINTER!", e);
                    Thread.currentThread().interrupt();
                }
            }
            Looper.loop();
        }

        /**
         * Send message to client.
         * @param msg Message to send to client. Must be a one-liner.
         */
        private void printOut(String msg) {
            output.print(msg + "\n");
            output.flush();
        }

    }

    /**
     * Used to make the connection stay alive. To avoid the android system to enter some
     * standby or unload the app from ram. Therefore, the system sends a heartbeat every
     * 3 seconds. Every 10th second, the media files of TheatreMagic.java will be reloaded
     * to memory (if not currently being used).
     */
    class KeepAliveThread implements Runnable {

        private Socket clientSocket;
        private PrintWriter output;
        private long lastHeartBeat = System.currentTimeMillis();

        /**
         * Constructor.
         * @param clientSocket the client tcp socket to send the heartbeat to.
         */
        public KeepAliveThread(Socket clientSocket) {
            this.clientSocket = clientSocket;

            try {
                this.output = new PrintWriter(this.clientSocket.getOutputStream(), true);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        /**
         * Performs the actual keep-alive-task: sends a heartbeat every 3rd second.
         */
        public void run() {
            /**
             * Looper-Crap is needed for multithreading on android.
             */
            Looper.prepare();

            while (!Thread.currentThread().isInterrupted()) {
                try {
                    long currentTime = System.currentTimeMillis();
                    if ( (currentTime-lastHeartBeat) > 3000 ) {
                        /**
                         * Check if media files should be reloaded. Handled by TheatreMagic.java
                         */
                        if ( (currentTime-lastHeartBeat) > 10000) {
                            //tm.stayReady();       // currently makes the app crash when phone
                                                    // locks the screen (HTC Desire)
                        }
                        /**
                         * Send heartbeat to client.
                         */
                        lastHeartBeat = currentTime;
                        printOut("HB " + lastHeartBeat);
                    }
                } catch (NullPointerException e) {
                    /**
                     * This mainly occurs when the client window is being closed without properly
                     * shutting down the tcp-connection.
                     */
                    Log.e("NETWORKING", "LOST CONNECTION NULLPOINTER!", e);
                    Thread.currentThread().interrupt();
                }
            }
            Looper.loop();
        }

        /**
         * Send message to client.
         * @param msg Message to send to client. Must be a one-liner.
         */
        private void printOut(String msg) {
            output.print(msg + "\n");
            output.flush();
        }

    }


}
