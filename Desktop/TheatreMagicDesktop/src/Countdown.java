import com.illposed.osc.OSCListener;
import com.illposed.osc.OSCMessage;
import com.illposed.osc.OSCPortIn;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.SocketException;
import java.util.Date;

/**
 * Created by mirko on 01.04.14.
 */
public class Countdown extends JFrame{
    private JLabel lblSeconds;
    private JPanel panelMain;
    private JButton btnMenu;

    public static final Dimension size = new Dimension(320,240);
    private OSCPortIn receiver;

    public static void main(String[] args) {

        Countdown c = new Countdown();
    }

    public Countdown() {
        super ("Cue Countdown");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setContentPane(panelMain);
        panelMain.setBackground(Color.WHITE);
        panelMain.setPreferredSize(size);
        panelMain.setMaximumSize(size);
        btnMenu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                CountdownMenu cm = new CountdownMenu();
            }
        });
        try {
            OSCPortIn receiver = new OSCPortIn(5400);

            OSCListener listener5 = new OSCListenerTime(5);
            OSCListener listener8 = new OSCListenerTime(8);
            OSCListener listener30 = new OSCListenerTime(30);
            OSCListener listener20 = new OSCListenerTime(20);
            OSCListener listener10 = new OSCListenerTime(10);

            receiver.addListener("/t8", listener8);
            receiver.addListener("/t5", listener5);
            receiver.addListener("/t30", listener30);
            receiver.addListener("/t20", listener20);
            receiver.addListener("/t10", listener10);

            receiver.startListening();
        }
        catch (SocketException e) {
            e.printStackTrace();
        }
        pack();
        setVisible(true);
    }
    

    protected class OSCListenerTime implements OSCListener {
        protected int timeDown = 0;

        public OSCListenerTime(int t) {
            super();
            timeDown = t;
        }

        public void acceptMessage(java.util.Date time, OSCMessage message) {
            ShowTimer st = new ShowTimer(new Date().getTime(), timeDown);
            Thread timer = new Thread(st);
            timer.start();
        }
    }

    

    SwingWorker<Void, Void> checkHeartbeat = new SwingWorker<Void, Void>() {

        @Override
        protected Void doInBackground() throws Exception{
            while (!this.isCancelled()) {

            }
            return null;
        }

    };

    protected class ShowTimer implements Runnable {

        double timeCodeStart;
        int maxCountdown;

        public ShowTimer(double t, int m) {
            timeCodeStart = t / 1000;
            maxCountdown = m;
            panelMain.setBackground(Color.WHITE);
        }

        @Override
        public void run() {
            double lastTime = 0.0;

            while (!Thread.currentThread().isInterrupted()) {

                double currentTime = new Date().getTime() / 1000;

                Double timeDiff = maxCountdown + (timeCodeStart - currentTime);

                if (timeDiff != lastTime) {
                    Integer diff = timeDiff.intValue();
                    System.out.println(currentTime + " - Noch " + diff.toString() + "s");
                    lblSeconds.setText(diff.toString());
                    lastTime = timeDiff;
                }

                if (timeDiff.intValue() <= 0) {
                    Thread.currentThread().interrupt();
                }
            }
            System.out.println("DO IT!!");
            lblSeconds.setText("GO");
            panelMain.setBackground(Color.GREEN);
        }
    }

}

