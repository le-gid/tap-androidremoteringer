import javax.swing.*;
import java.awt.*;

/**
 * Created by mirko on 03.04.14.
 */
public class CountdownMenu extends JFrame{
    private JPanel panelDebug;
    private JPanel panelSystemMenu;
    private JTabbedPane paneTabs;
    private JButton button1;
    private JButton button2;

    public CountdownMenu() {
        super ("Cue Countdown");
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        setContentPane(paneTabs);
        paneTabs.setBackground(Color.WHITE);
        paneTabs.setPreferredSize(Countdown.size);
        paneTabs.setMaximumSize(Countdown.size);
        pack();
        setVisible(true);
    }
}
