import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.EventQueue;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JLabel;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JSplitPane;
import javax.swing.JScrollPane;
import javax.swing.JInternalFrame;
import javax.swing.JTextPane;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;
import javax.swing.JList;
import javax.swing.AbstractListModel;
import javax.swing.JTextArea;
import javax.swing.JEditorPane;
import javax.swing.UIManager;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.FlowLayout;


public class GUIWindow extends JFrame {

	private JPanel contentPane;
	private Player p;
	private JTextField txtIPAddress;
	private String ipAddress;
	private JButton btnConnect;
	private ActionListener alConnect;
	private ActionListener alDisconnect;
	
	Logger l;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUIWindow frame = new GUIWindow();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GUIWindow() {
		alConnect = new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				ipAddress = txtIPAddress.getText();
				connectToServer(btnConnect);
				
			}
		};
		
		alDisconnect = new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				disconnectFromServer(btnConnect);
				
			}
		};
		
		Action aPlay = new AbstractAction() {
			
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				if (p != null && p.isConnected()) {
					p.play();
				}
				else {
					l.error("Not connected to smartphone!");
				}
				
			}
		};
		
		Action aStop = new AbstractAction() {
			
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				if (p != null && p.isConnected()) {
					p.stop();
				}
				else {
					l.error("Not connected to smartphone!");
				}
				
			}
		};
		
		ActionListener alPlay1 = new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if (p != null && p.isConnected()) {
					p.play();
				}
				else {
					l.error("Not connected to smartphone!");
				}
				
			}
		};
		
		ActionListener alStop = new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if (p != null && p.isConnected()) {
					p.stop();
				}
				else {
					l.error("Not connected to smartphone!");
				}
				
			}
		};
		
		
		setTitle("Theatre Magic Remote Control");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 483, 380);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new GridLayout(1, 0, 0, 0));
		
		JSplitPane splitPane = new JSplitPane();
		splitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
		contentPane.add(splitPane);
		
		JPanel panel_controls = new JPanel();
		splitPane.setLeftComponent(panel_controls);
		
		JPanel panel_heartbeat = new JPanel();
		panel_heartbeat.setBorder(UIManager.getBorder("CheckBox.border"));
		panel_heartbeat.setBackground(Color.WHITE);
		FlowLayout fl_panel_heartbeat = (FlowLayout) panel_heartbeat.getLayout();
		fl_panel_heartbeat.setVgap(30);
		fl_panel_heartbeat.setHgap(30);
		panel_controls.add(panel_heartbeat);
		
		JSplitPane splitPane_controls = new JSplitPane();
		splitPane_controls.setOrientation(JSplitPane.VERTICAL_SPLIT);
		panel_controls.add(splitPane_controls);
		
		JPanel panel_connection = new JPanel();
		splitPane_controls.setLeftComponent(panel_connection);
		
		JLabel lblIpAddress = new JLabel("IP Address:");
		panel_connection.add(lblIpAddress);
		
		txtIPAddress = new JTextField();
		panel_connection.add(txtIPAddress);
		txtIPAddress.setColumns(10);
		
		btnConnect = new JButton("Connect");
		panel_connection.add(btnConnect);
		btnConnect.addActionListener(alConnect);

		
		JPanel panel_playButtons = new JPanel();
		splitPane_controls.setRightComponent(panel_playButtons);
		
		JButton btnPlay = new JButton("Play");
		panel_playButtons.add(btnPlay);
		
		JButton btnStop = new JButton("Stop");
		panel_playButtons.add(btnStop);
		
		JButton btnKeybinding = new JButton("W=Play - E=Stop");
		panel_playButtons.add(btnKeybinding);
		
		btnStop.addActionListener(alStop);
		btnPlay.addActionListener(alPlay1);
		
		btnKeybinding.getInputMap().put(KeyStroke.getKeyStroke("W"), "playRing");
		btnKeybinding.getActionMap().put("playRing", aPlay);
		
		btnKeybinding.getInputMap().put(KeyStroke.getKeyStroke("E"), "stopRing");
		btnKeybinding.getActionMap().put("stopRing", aStop);
		
		JPanel panel_log = new JPanel();
		splitPane.setRightComponent(panel_log);
		panel_log.setLayout(new GridLayout(0, 1, 0, 0));
		
		JEditorPane txtLogging = new JEditorPane();
		txtLogging.setContentType("text/html");
		panel_log.add(txtLogging);
		

		l = new Logger(txtLogging, panel_heartbeat);
		l.log("TMRC started");
		l.checkHeartbeat.execute();
	}
	
	private void connectToServer(JButton referenceBtn) {
		p = new Player(ipAddress, l);
		if (p.isConnected()) {
			l.log("Connection established");
			referenceBtn.removeActionListener(alConnect);
			referenceBtn.addActionListener(alDisconnect);
			referenceBtn.setText("Disconnect");
		}
		else {
			l.error("Unable to connect to " + ipAddress + ":6000");
		}
	}

	private void disconnectFromServer(JButton referenceBtn) {
		if (p.isConnected()) {
			p.disconnect();
			referenceBtn.removeActionListener(alDisconnect);
			referenceBtn.addActionListener(alConnect);
			referenceBtn.setText("Connect");
		}
		else {
			l.error("Unable to disconnect.");
		}
	}
}
