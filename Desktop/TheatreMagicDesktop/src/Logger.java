import java.awt.Color;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;

import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingWorker;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.StyleSheet;


public class Logger {
	
	private JEditorPane txtLog;
	private ArrayList<String> logs = new ArrayList<String>();
	private HTMLEditorKit editKit = new HTMLEditorKit();
	private StyleSheet style;
	private String heartbeat = "No heartbeat received yet.";
	private JPanel panel_heartbeat;
	private Long lastHeartbeat;
	public boolean stopHeartbeat = false;
	
	public Logger(JEditorPane t, JPanel j) {
		txtLog = t;
		panel_heartbeat = j;
		style = editKit.getStyleSheet();
		style.addRule("body{font-family: sans; font-size: 12pt;}");
		style.addRule("#err{color:red}");
		style.addRule("#ok{color:green}");
		txtLog.setDocument(editKit.createDefaultDocument());
		txtLog.setEditorKit(editKit);
		txtLog.setEditable(false);
	}
	
	public void log(String msg) {
		log(msg, "I");
	}
	
	public void log(String msg, String prefix) {
		logs.add(new String(prefix + " " + (new SimpleDateFormat("[dd.MM.yy HH:mm:ss] ").format(new Date()) + " " + msg + "<br>"))); 
		createLogFromArrayList();
	}
	
	public void serverMessage(String msg) {
		String statusCode = msg.substring(msg.length()-2, msg.length());
		
		if (statusCode.equals("OK")) {
			msg = "<span id=\"ok\">" + msg + "</span>";
		}
		else if (statusCode.equals("ER")) {
			msg = "<span id=\"err\">" + msg + "</span>";
		}
		
		log(msg, "S");
	}
	
	public void error(String msg) {
		log(msg + "</span>" , "<span id=\"err\">E");
	}
	
	public void heartbeat(String msg) {
		heartbeat = msg;
		lastHeartbeat = System.currentTimeMillis();
		createLogFromArrayList();
	}
	
	private void createLogFromArrayList() {
		StringBuilder log = new StringBuilder();
		log.append(heartbeat);
		log.append("<br><br>");
		ArrayList<String> logs_reversed = (ArrayList<String>) logs.clone();
		Collections.reverse(logs_reversed);
		
		for(String logEntry : logs_reversed) {
			log.append(logEntry);
		}
		txtLog.setText(log.toString());
	}
	
	SwingWorker<Void, Void> checkHeartbeat = new SwingWorker<Void, Void>() {

		@Override
		protected Void doInBackground() throws Exception{
			log("Starting Heartbeat-o-meter");
	        while (!this.isCancelled()) {
				log ("Still there. .");
				if ((System.currentTimeMillis() - lastHeartbeat) < 4000) {
					panel_heartbeat.setBackground(Color.GREEN);
					log ("All good");
				}
				else {
					panel_heartbeat.setBackground(Color.RED);
					error("Last heartbeat: " + (System.currentTimeMillis()-lastHeartbeat) + "s ago!");
				}
				Thread.currentThread().sleep(500);
			}
	        return null;
		}
		
	};
}
