import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ConnectException;
import java.net.Socket;

import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;


public class Player {

	private final int port = 6000;
	private Socket socket;
	private PrintWriter output;
	private BufferedReader input;
	private Logger log;
	
	public Player(String ip, Logger l) {
		log = l;
		
		try {
			socket = new Socket(ip, port);
			output = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));
			input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			worker.execute();
		} catch (IOException e) {
			log.error("Unable to connect to server!");
		}
	}
	
	public boolean isConnected() {
		if (socket == null) {
			return false;
		}
		return socket.isConnected();
	}
	
	public void play() {
		write("play");
	}
	
	public void stop() {
		write("stop");
	}
	
	public void disconnect() {
		write("exit");
		try {
			worker.cancel(true);
			socket.close();
			log.log("Connection destroyed.");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void write(String command) {
		log.log(command);
		output.print(command + "\n");
		output.flush();
	}
	
	SwingWorker<Void, Void> worker = new SwingWorker<Void, Void>() {

		@Override
		protected Void doInBackground() throws Exception {
			while (socket.isConnected()) {
				try {
					String in = input.readLine();
					if (in == null) {
						continue;
					}
					System.out.println(in);
					if (in.length() > 1 && !(in.substring(0, 2).equals("HB"))) {
						log.serverMessage(in);
					}
					else {
						log.heartbeat(in);
					}
				} catch (IOException e) {
					log.error("F#ck it!");
					continue;
				}
				
			}
			return null;
		}
		
	};
}
