import com.illposed.osc.*;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;

public class QLabDebug {

	public static void main(String[] args) throws IOException {
        System.out.println("Server started!");
        OSCPortIn receiver = new OSCPortIn(5400);
        OSCListener listener = new OSCListener() {

            public void acceptMessage(java.util.Date time, OSCMessage message) {
                System.out.println("3 Seconds left!");
                ShowTimer st = new ShowTimer(new Date().getTime());
                Thread timer = new Thread(st);
                timer.start();
            }
        };
        receiver.addListener("/3secondsToMars", listener);
        receiver.startListening();
	}

    static class ShowTimer implements Runnable {

        double timeCodeStart;

        public ShowTimer(double t) {
            timeCodeStart = t / 1000;
        }

        @Override
        public void run() {
            double lastTime = 0.0;

            while(!Thread.currentThread().isInterrupted()) {

                double currentTime = new Date().getTime() / 1000;

                Double timeDiff = 6 + (timeCodeStart - currentTime);

                if (timeDiff != lastTime) {
                    System.out.println(currentTime + " - Noch " + timeDiff.intValue() + "s");
                    lastTime = timeDiff;
                }

                if ( timeDiff.intValue() <= 0 ) {
                    Thread.currentThread().interrupt();
                }
            }
            System.out.println("DO IT!!");
        }
    }

}
