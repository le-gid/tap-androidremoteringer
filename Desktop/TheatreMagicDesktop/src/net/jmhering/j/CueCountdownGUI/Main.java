package net.jmhering.j.CueCountdownGUI;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingWorker;

import java.awt.BorderLayout;
import javax.swing.JLabel;
import java.awt.GridLayout;
import javax.swing.BoxLayout;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.net.SocketException;
import java.util.Date;

import javax.swing.JButton;

import com.illposed.osc.OSCListener;
import com.illposed.osc.OSCMessage;
import com.illposed.osc.OSCPortIn;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Main {

	private JFrame frame;
	protected JPanel panelMain;
	protected JLabel lblSeconds;
	public static final int height = 240;
	public static final int width = 320;
	
	public boolean serverRunning = false;
	public int serverPort = 5400;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Main window = new Main();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Main() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(0, 0, Main.width, Main.height);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		panelMain = new JPanel();
		frame.getContentPane().add(panelMain, BorderLayout.CENTER);
		panelMain.setLayout(new BorderLayout(0, 0));
		
		JLabel lblNewLabel = new JLabel("Seconds to the next cue:");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		panelMain.add(lblNewLabel, BorderLayout.NORTH);
		
		lblSeconds = new JLabel("---");
		lblSeconds.setFont(new Font("Dialog", Font.BOLD, 72));
		lblSeconds.setHorizontalAlignment(SwingConstants.CENTER);
		panelMain.add(lblSeconds, BorderLayout.CENTER);
		
		JButton btnMenu = new JButton("Menu");
		btnMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Menu m = new Menu(Main.this);
				m.setVisible(true);
				m.frame.setVisible(true);
			}
		});
		panelMain.add(btnMenu, BorderLayout.SOUTH);
		startServer();
	}
	
	private void startServer() {
		 try {
	            OSCPortIn receiver = new OSCPortIn(serverPort);
	            
	            OSCListener listener5 = new OSCListenerTime(5);
	            OSCListener listener8 = new OSCListenerTime(8);
	            OSCListener listener30 = new OSCListenerTime(30);
	            OSCListener listener20 = new OSCListenerTime(20);
	            OSCListener listener10 = new OSCListenerTime(10);

	            receiver.addListener("/t8", listener8);
	            receiver.addListener("/t5", listener5);
	            receiver.addListener("/t30", listener30);
	            receiver.addListener("/t20", listener20);
	            receiver.addListener("/t10", listener10);

	            receiver.startListening();
	            this.serverRunning = true;
	            System.out.println ("Started OSC server");
	        }
		 
	        catch (SocketException e) {
	            e.printStackTrace();
	            System.out.println ("Error starting the OSC server!");
	            this.serverRunning = false;
	        }
	}
	


    protected class OSCListenerTime implements OSCListener {
        protected int timeDown = 0;

        public OSCListenerTime(int t) {
            super();
            timeDown = t;
        }

        public void acceptMessage(java.util.Date time, OSCMessage message) {
            ShowTimer st = new ShowTimer(new Date().getTime(), timeDown);
            Thread timer = new Thread(st);
            timer.start();
        }
    }

    

    SwingWorker<Void, Void> checkHeartbeat = new SwingWorker<Void, Void>() {

        @Override
        protected Void doInBackground() throws Exception{
            while (!this.isCancelled()) {

            }
            return null;
        }

    };

    protected class ShowTimer implements Runnable {

        double timeCodeStart;
        int maxCountdown;

        public ShowTimer(double t, int m) {
            timeCodeStart = t / 1000;
            maxCountdown = m;
            panelMain.setBackground(Color.WHITE);
        }

        @Override
        public void run() {
            double lastTime = 0.0;

            while (!Thread.currentThread().isInterrupted()) {

                double currentTime = new Date().getTime() / 1000;

                Double timeDiff = maxCountdown + (timeCodeStart - currentTime);

                if (timeDiff != lastTime) {
                    Integer diff = timeDiff.intValue();
                    System.out.println(currentTime + " - Noch " + diff.toString() + "s");
                    lblSeconds.setText(diff.toString());
                    lastTime = timeDiff;
                }

                if (timeDiff.intValue() <= 0) {
                    Thread.currentThread().interrupt();
                }
            }
            System.out.println("DO IT!!");
            lblSeconds.setText("GO");
            panelMain.setBackground(Color.GREEN);
        }
    }

}
