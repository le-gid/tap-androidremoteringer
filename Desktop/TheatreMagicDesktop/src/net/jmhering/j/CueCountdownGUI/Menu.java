package net.jmhering.j.CueCountdownGUI;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.BoxLayout;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Menu extends JFrame{

	public JFrame frame;
	private Main rootWindow;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Menu window = new Menu(new Main());
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Menu(Main r) {
		rootWindow = r;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(0, 0, Main.width, Main.height);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JPanel panel = new JPanel();
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		
		JButton btnSystem = new JButton("System");
		btnSystem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MenuSystem m = new MenuSystem();
				m.frame.setVisible(true);
			}
		});
		panel.setLayout(new GridLayout(0, 2, 0, 0));
		panel.add(btnSystem);
		
		JButton btnInfo = new JButton("Info");
		btnInfo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				WindowInformation i = new WindowInformation(rootWindow);
				i.frame.setVisible(true);
			}
		});
		panel.add(btnInfo);
		
		JButton btnBack = new JButton("Back");
		frame.getContentPane().add(btnBack, BorderLayout.EAST);
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.setVisible(false);
			}
		});
	}

}
