package net.jmhering.j.CueCountdownGUI;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.BorderLayout;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

import javax.swing.JLabel;
import javax.swing.JTextPane;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class WindowInformation {

	public JFrame frame;
	private Main rootWindow;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					WindowInformation window = new WindowInformation(new Main());
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public WindowInformation(Main r) {
		rootWindow = r;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(0, 0, Main.width, Main.height);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JButton btnNewButton = new JButton("Back");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.setVisible(false);
			}
		});
		frame.getContentPane().add(btnNewButton, BorderLayout.EAST);
		
		JTextPane textPane = new JTextPane();
		textPane.setContentType("text/html");
		frame.getContentPane().add(textPane, BorderLayout.CENTER);
		
		getInformation(textPane);
	}
	
	private void getInformation(JTextPane t) {
		StringBuilder output = new StringBuilder();
		String networkFilter = "147.";
		try {
			Enumeration<NetworkInterface> ifaces = NetworkInterface.getNetworkInterfaces();
			
			while (ifaces.hasMoreElements()) {
				NetworkInterface n=(NetworkInterface) ifaces.nextElement();
				Enumeration<InetAddress> ifaceAddresses = n.getInetAddresses();
				
				while (ifaceAddresses.hasMoreElements()) {
					InetAddress i = (InetAddress) ifaceAddresses.nextElement();
					
					if (i.getHostAddress().substring(0,networkFilter.length()).equals(networkFilter)) {
						output.append("IP: " + i.getHostAddress() + "<br>");
					}
				}
			}
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			output.append("Unable to fetch network devices.<br>");
		}
		
		if (rootWindow.serverRunning) {
			output.append("Server running on port " + rootWindow.serverPort + "<br>");
		}
		else {
			output.append("Server not running!<br>");
		}
		
		t.setText(output.toString());
	}

}
